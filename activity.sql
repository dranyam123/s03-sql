mysql -u root -p

CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE Users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Posts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(50) NOT NULL,
    content VARCHAR(50) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_Users_user_id
        FOREIGN KEY (user_id) REFERENCES Users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


-- Users 
INSERT INTO Users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO Users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");

INSERT INTO Users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");

INSERT INTO Users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

INSERT INTO Users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


-- Posts
INSERT INTO Posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");

INSERT INTO Posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");

INSERT INTO Posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");

INSERT INTO Posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Get all the post of Author Id 1
SELECT * FROM Posts WHERE user_id = 1;

-- Get all the user email and date time creation
SELECT email, datetime_created FROM Users;

-- Update post
UPDATE Posts SET content = "Hello to the people of the Earth" WHERE id = 2;

-- Delete
DELETE FROM Users WHERE email = "johndoe@gmail.com";